<?php
  ob_start();
  header('Content-Type: text/html; charset=utf-8');
  ini_set("diplay_errors", 1);
  require_once "classes/Database.php";
  require_once "classes/JoomlaImporter.php";
  require_once "classes/SimpleHtmlDom.php";
  require_once "classes/Parser.php";
  require_once "functions.php";
  $categories = require_once "config.php";

  $dataBase = new Database("localhost", "u0281236_clim", "random", "ksushavlad1907", "utf8", true);

  $parser = new Parser;
  $parser->setSiteUrl("https://www.rusklimat.ru");
  $parser->setProductHandler("#catalog_items .item");
  $parser->setNameHandler(".ttl");
  $parser->setDescriptionHandler("article[itemprop=description]", true);
  $parser->setPriceHandler(".curr");
  $parser->setImageHandler(".catalog-element__photo img", true, "img/");
  $parser->setSkuHandler(".article", true);
  $parser->setAttributesHandler(".tab-char", true, true, "specification/");
  $parser->setPaginationHandler(".navigation-pages");
  $parser->setPaginationCurrentHandler("li.active");
  $parser->setLinkHandler(".ttl a");

  $joomlaImporter = new JoomlaImporter($dataBase);
  $joomlaImporter->setTablePrefix("clm7_");

  foreach ($categories as $catName => $catUrl) {
    ?><pre><?php
    var_dump($catName);
    $category = new JoomlaCategory;
    $category->setName($catName);
    $category->setSlug(str2url($catName));

    $joomlaImporter->setCategory($category);

    $joomlaImporter->importCategory(false, "name");

    if (!$category->getId()) {
      print "<br>Пропущено: $catName<br>";
      ob_flush();
      flush();
      continue;
    }

    $parser->setWorkingPage($catUrl);
    $parser->parse();
    $products = $parser->getProducts();

    ?><pre><?php
    continue;

    for ($i = 0; $i < count($products); $i++) {
      $products[$i]['name'] = trim($products[$i]['name']);
      $products[$i]['description'] = trim($products[$i]['description']);

      $price = explode("	", $products[$i]['price']);
      $products[$i]['price'] = intval($price[0]);

      $sku = explode("код товара:", $products[$i]['sku']);
      $products[$i]['sku'] = trim($sku[1]);

      $attributes = $products[$i]['attributes'];

      $pattern = "/<td>([^><]*)/";
      preg_match_all($pattern, $attributes, $parametersNames, PREG_PATTERN_ORDER);

      $pattern = "/<td [style=\"width:160px\"]*>([^><]*)/";
      preg_match_all($pattern, $attributes, $parametersValues, PREG_PATTERN_ORDER);

      $attributes = array();
      $j = 0;
      foreach ($parametersNames[0] as $name) {
        $attributes[] = array($name, trim($parametersValues[0][$j], "<td>"));
        $j++;
      }
      $products[$i]['attributes'] = $attributes;

      $image = new JoomlaImage;
      $image->setTitle($products[$i]['image']);
      $image->setDescription($products[$i]['name']);
      $image->checkType();
      $image->setUrl("images/virtuemart/product/" . $image->getTitle());

      $j = 0;
      foreach ($products[$i]['attributes'] as $customfield) {
        $customfields[$j] = new JoomlaCustomfield;
        $customfields[$j]->setTitle($customfield[0]);
        $customfields[$j]->setProductCustomValue($customfield[1]);
        $j++;
      }

      $product = new JoomlaProduct;
      $product->setName($products[$i]['name']);
      $product->setDesc($products[$i]["description"]);
      $product->setSku($products[$i]['sku']);
      $product->setSlug(str2url($products[$i]['name']), 192, true);
      $product->setPrice($products[$i]['price']);
      $product->setCategoryId($category->getId());

      $joomlaImporter->setProduct($product);
      $joomlaImporter->setImage($image);
      $joomlaImporter->setCustomfields($customfields);

      $joomlaImporter->importImage(false, "title");
      $product->setMediaId($image->getId());

      $joomlaImporter->importCustomfields(false, "title");
      $product->setCustomfields($customfields);

      $product->setCategoryId(23);
      $joomlaImporter->importProduct(false, "name");
    }

    print "<br>Закончена работа с $catName. Количество товаров: " . count($products) . "<br>";
    ob_flush();
    flush();
    die;
  }

  print "<h1>DONE</h1>";
?>
