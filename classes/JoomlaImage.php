<?php
  class JoomlaImage {
    private $id = NULL;
    private $vendorId = 1;
    private $title = "";
    private $description = "";
    private $meta = "";
    private $class = "";
    private $mimetype = "image/jpeg";
    private $type = "product";
    private $url = "";
    private $urlThumb = "";
    private $isProductImage = 0;
    private $isDownloable = 0;
    private $isForSale = 0;
    private $params = "";
    private $lang = "";
    private $shared = 0;
    private $published = 1;
    private $createdOn;
    private $createdBy = 0;
    private $modifiedOn;
    private $modifiedBy = 0;
    private $lockedOn;
    private $lockedBy = 0;

    public function __construct() {
      $this->createdOn = date("Y-m-d h:i:s");
      $this->modifiedOn = date("Y-m-d h:i:s");
      $this->lockedOn = strtotime("0000-00-00 00:00:00");
    }

    public function checkType() {
      if (!$this->title) {
        $this->mimetype = "image/jpeg";
        return false;
      }

      if (substr($this->title, -4) == "jpeg") {
        $this->mimetype = "image/jpeg";
      } else if (substr($this->title, -3) == "jpg") {
        $this->mimetype = "image/jpg";
      } else if (substr($this->title, -3) == "png") {
        $this->mimetype = "image/png";
      }

      return $this->mimetype;
    }

    public function setId($id)  {
      $this->id = $id;
    }

    public function setVendorId($vendorId)  {
      $this->vendorId = $vendorId;
    }

    public function setTitle($title)  {
      $this->title = $title;
    }

    public function setDescription($description)  {
      $this->description = $description;
    }

    public function setMeta($meta)  {
      $this->meta = $meta;
    }

    public function setClass($class)  {
      $this->class = $class;
    }

    public function setMimetype($mimetype)  {
      $this->mimetype = $mimetype;
    }

    public function setType($type)  {
      $this->type = $type;
    }

    public function setUrl($url)  {
      $this->url = $url;
    }

    public function setUrlThumb($urlThumb)  {
      $this->urlThumb = $urlThumb;
    }

    public function setIsProductImage($isProductImage)  {
      $this->isProductImage = $isProductImage;
    }

    public function setIsDownloable($isDownloable)  {
      $this->isDownloable = $isDownloable;
    }

    public function setIsForSale($isForSale)  {
      $this->isForSale = $isForSale;
    }

    public function setParams($params)  {
      $this->params = $params;
    }

    public function setLang($lang)  {
      $this->lang = $lang;
    }

    public function setShared($shared)  {
      $this->shared = $shared;
    }

    public function setPublished($published)  {
      $this->published = $published;
    }

    public function setCreatedOn($createdOn)  {
      $this->createdOn = $createdOn;
    }

    public function setCreatedBy($createdBy)  {
      $this->createdBy = $createdBy;
    }

    public function setModifiedOn($modifiedOn)  {
      $this->modifiedOn = $modifiedOn;
    }

    public function setModifiedBy($modifiedBy)  {
      $this->modifiedBy = $modifiedBy;
    }

    public function setLockedOn($lockedOn)  {
      $this->lockedOn = $lockedOn;
    }

    public function setLockedBy($lockedBy)  {
      $this->lockedBy = $lockedBy;
    }

    //getters
    public function getId()  {
      return $this->id;
    }

    public function getVendorId()  {
      return $this->vendorId;
    }

    public function getTitle()  {
      return $this->title;
    }

    public function getDescription()  {
      return $this->description;
    }

    public function getMeta()  {
      return $this->meta;
    }

    public function getClass()  {
      return $this->class;
    }

    public function getMimetype()  {
      return $this->mimetype;
    }

    public function getType()  {
      return $this->type;
    }

    public function getUrl()  {
      return $this->url;
    }

    public function getUrlThumb()  {
      return $this->urlThumb;
    }

    public function getIsProductImage()  {
      return $this->isProductImage;
    }

    public function getIsDownloable()  {
      return $this->isDownloable;
    }

    public function getIsForSale()  {
      return $this->isForSale;
    }

    public function getParams()  {
      return $this->params;
    }

    public function getLang()  {
      return $this->lang;
    }

    public function getShared()  {
      return $this->shared;
    }

    public function getPublished()  {
      return $this->published;
    }

    public function getCreatedOn()  {
      return $this->createdOn;
    }

    public function getCreatedBy()  {
      return $this->createdBy;
    }

    public function getModifiedOn()  {
      return $this->modifiedOn;
    }

    public function getModifiedBy()  {
      return $this->modifiedBy;
    }

    public function getLockedOn()  {
      return $this->lockedOn;
    }

    public function getLockedBy()  {
      return $this->lockedBy;
    }
  }
?>
