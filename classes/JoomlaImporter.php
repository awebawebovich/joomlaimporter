<?php
  require_once "JoomlaCategory.php";
  require_once "JoomlaProduct.php";
  require_once "JoomlaImage.php";
  require_once "JoomlaCustomfield.php";

  class JoomlaImporter {
    private $database;
    private $tablePrefix;
    private $product = NULL;
    private $category = NULL;
    private $image = NULL;
    private $customfields = NULL;

    public function __construct($database) {
      $this->database = $database;
    }

    public function setTablePrefix($tablePrefix) {
      $this->tablePrefix = $tablePrefix;
    }

    public function setProduct($product) {
      if (is_object($product)) {
        $this->product = $product;
      } else {
        $this->product = false;
      }
    }

    public function setCategory($category) {
      if (is_object($category)) {
        $this->category = $category;
      } else {
        $this->category = false;
      }
    }

    public function setImage($image) {
      if (is_object($image)) {
        $this->image = $image;
      } else {
        $this->image = false;
      }
    }

    public function setCustomfields($customfields) {
      if (is_array($customfields)) {
        $this->customfields = $customfields;
      } else {
        $this->customfields = false;
      }
    }

    public function importImage($updated = true, $compareField = false) {
      if (!$this->database) {
        return false;
      }

      if ($compareField) {
        $check = $this->checkImage($compareField);

        if ($check && $updated) {
          $this->updateImage();
        } else if ($check && !$updated) {
          return false;
        } else {
          $this->image->setId(intval($this->insertImage()));
        }
      } else {
        $this->image->setId(intval($this->insertImage()));
      }
    }

    private function checkImage($compareField) {
      $functionName = "get" . ucfirst($compareField);
      $this->database->setQuery("SELECT * FROM {$this->tablePrefix}virtuemart_medias WHERE file_$compareField = ?");
      $this->database->setParameters(array($this->image->$functionName()));

      if ($row = $this->database->run()->fetchObj()->getRow()) {
        $this->image->setId($row->virtuemart_media_id);
        return true;
      } else {
        return false;
      }
    }

    private function updateImage() {

    }

    private function insertImage() {
      $this->database->setQuery("INSERT INTO {$this->tablePrefix}virtuemart_medias VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
      $this->database->setParameters(array(
        $this->image->getId(),
        $this->image->getVendorId(),
        $this->image->getTitle(),
        $this->image->getDescription(),
        $this->image->getMeta(),
        $this->image->getClass(),
        $this->image->getMimetype(),
        $this->image->getType(),
        $this->image->getUrl(),
        $this->image->getUrlThumb(),
        $this->image->getIsProductImage(),
        $this->image->getIsDownloable(),
        $this->image->getIsForSale(),
        $this->image->getParams(),
        $this->image->getLang(),
        $this->image->getShared(),
        $this->image->getPublished(),
        $this->image->getCreatedOn(),
        $this->image->getCreatedBy(),
        $this->image->getModifiedOn(),
        $this->image->getModifiedBy(),
        $this->image->getLockedOn(),
        $this->image->getLockedBy(),
      ));

      return $this->database->run()->getLastInsertId();
    }

    public function importCustomfields($updated = true, $compareField = false) {
      if (!$this->database) {
        return false;
      }

      if ($compareField) {
        $this->checkCustomfields($updated, $compareField);
      } else {
        for ($i = 0; $i < count($this->customfields); $i++) {
          $this->insertCustomfield($i);
        }
      }
    }

    private function checkCustomfields($updated, $compareField) {
      for($i = 0; $i < count($this->customfields); $i++) {
        $functionName = "get" . ucfirst($compareField);
        $this->database->setQuery("SELECT * FROM {$this->tablePrefix}virtuemart_customs WHERE custom_$compareField = ?");
        $this->database->setParameters(array($this->customfields[$i]->$functionName()));

        if ($row = $this->database->run()->fetchObj()->getRow()) {
          $this->customfields[$i]->setId($row->virtuemart_custom_id);

          if ($updated) {
            $this->updateCustomfield($i);
          } else {
            return false;
          }
        } else {
          $this->insertCustomfield($i);
        }
      }
    }

    private function insertCustomfield($index) {
      $this->database->setQuery("INSERT INTO {$this->tablePrefix}virtuemart_customs VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
      $this->database->setParameters(array(
        $this->customfields[$index]->getId(),
        $this->customfields[$index]->getParent(),
        $this->customfields[$index]->getVendorId(),
        $this->customfields[$index]->getJpluginId(),
        $this->customfields[$index]->getElement(),
        $this->customfields[$index]->getAdminOnly(),
        $this->customfields[$index]->getTitle(),
        $this->customfields[$index]->getShowTitle(),
        $this->customfields[$index]->getTip(),
        $this->customfields[$index]->getValue(),
        $this->customfields[$index]->getDesc(),
        $this->customfields[$index]->getFieldType(),
        $this->customfields[$index]->getIsList(),
        $this->customfields[$index]->getIsHidden(),
        $this->customfields[$index]->getIsCartAttribute(),
        $this->customfields[$index]->getIsInput(),
        $this->customfields[$index]->getSearchable(),
        $this->customfields[$index]->getLayoutPos(),
        $this->customfields[$index]->getParams(),
        $this->customfields[$index]->getShared(),
        $this->customfields[$index]->getPublished(),
        $this->customfields[$index]->getCreatedOn(),
        $this->customfields[$index]->getCreatedBy(),
        $this->customfields[$index]->getOrdering(),
        $this->customfields[$index]->getModifiedOn(),
        $this->customfields[$index]->getModifiedBy(),
        $this->customfields[$index]->getLockedOn(),
        $this->customfields[$index]->getLockedBy(),
      ));

      $this->database->run();
      $this->customfields[$index]->setId(intval($this->database->getLastInsertId()));
      return $this->database->getLastInsertId();
    }

    private function updateCustomfield($index) {

    }

    public function importProduct($updated = true, $compareField = false) {
      if (!$this->database) {
        return false;
      }

      if ($compareField) {
        $check = $this->checkProduct($compareField);

        if ($check && $updated) {
          $this->updateProduct();
        } else if ($check && !$updated) {
          return false;
        } else {
          $this->insertProduct();
        }
      } else {
        $this->insertProduct();
      }
    }

    private function checkProduct($compareField) {
      $functionName = "get" . ucfirst($compareField);
      $this->database->setQuery("SELECT * FROM {$this->tablePrefix}virtuemart_products_ru_ru WHERE product_$compareField = ?");
      $this->database->setParameters(array($this->product->$functionName()));

      if ($row = $this->database->run()->fetchObj()->getRow()) {
        $this->product->setId($row->virtuemart_product_id);
        return true;
      } else {
        return false;
      }
    }

    private function insertProduct() {
      //main
      $this->insertProductMain();

      //meta
      $this->insertProductMeta();

      //prices
      $this->insertProductPrices();

      //medias
      $this->insertProductMedia();

      //customfields
      $this->insertProductCustomfields();

      //categories
      $this->insertProductCategory();
    }

    private function insertProductMain() {
      $this->database->setQuery("INSERT INTO {$this->tablePrefix}virtuemart_products VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
      $this->database->setParameters(array(
        $this->product->getId(),
        $this->product->getVendorId(),
        $this->product->getParent(),
        $this->product->getSku(),
        $this->product->getGtin(),
        $this->product->getMpn(),
        $this->product->getWeight(),
        $this->product->getWeightUom(),
        $this->product->getLength(),
        $this->product->getWidth(),
        $this->product->getHeight(),
        $this->product->getlwhUom(),
        $this->product->getUrl(),
        $this->product->getInStock(),
        $this->product->getOrdered(),
        $this->product->getStockhandle(),
        $this->product->getStockNotification(),
        $this->product->getAvailableData(),
        $this->product->getAvaibility(),
        $this->product->getSpecial(),
        $this->product->getDiscontinued(),
        $this->product->getSales(),
        $this->product->getUnit(),
        $this->product->getPacking(),
        $this->product->getParams(),
        $this->product->getHits(),
        $this->product->getIntnotes(),
        $this->product->getMetarobot(),
        $this->product->getMetaauthor(),
        $this->product->getLayout(),
        $this->product->getPublished(),
        $this->product->getPordering(),
        $this->product->getCreatedOn(),
        $this->product->getCreatedBy(),
        $this->product->getModifiedOn(),
        $this->product->getModifiedBy(),
        $this->product->getLockedOn(),
        $this->product->getLockedBy(),
      ));

      $this->database->run();
      $this->product->setId(intval($this->database->getLastInsertId()));
      return $this->database->getLastInsertId();
    }

    private function insertProductMeta() {
      if (!$this->product->getId()) {
        return false;
      }

      $this->database->setQuery("INSERT INTO {$this->tablePrefix}virtuemart_products_ru_ru VALUES (?,?,?,?,?,?,?,?)");
      $this->database->setParameters(array(
        $this->product->getId(),
        $this->product->getSDesc(),
        $this->product->getDesc(),
        $this->product->getName(),
        $this->product->getMetadesc(),
        $this->product->getMetakey(),
        $this->product->getCustomtitle(),
        $this->product->getSlug()
      ));

      $this->database->run();
    }

    private function insertProductPrices() {
      if (!$this->product->getId()) {
        return false;
      }

      $this->database->setQuery("INSERT INTO {$this->tablePrefix}virtuemart_product_prices VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
      $this->database->setParameters(array(
        NULL,
        $this->product->getId(),
        $this->product->getShoppergroupId(),
        $this->product->getPrice(),
        $this->product->getOverride(),
        $this->product->getOverridePrice(),
        $this->product->getTaxId(),
        $this->product->getDiscountId(),
        $this->product->getCurrency(),
        $this->product->getPricePublishUp(),
        $this->product->getPricePublishDown(),
        $this->product->getPriceQuantityStart(),
        $this->product->getPriceQuantityEnd(),
        $this->product->getCreatedOn(),
        $this->product->getCreatedBy(),
        $this->product->getModifiedOn(),
        $this->product->getModifiedBy(),
        $this->product->getLockedOn(),
        $this->product->getLockedBy()
      ));

      $this->database->run();
    }

    private function insertProductMedia() {
      if (!$this->product->getId()) {
        return false;
      }

      $this->database->setQuery("INSERT INTO {$this->tablePrefix}virtuemart_product_medias VALUES (?,?,?,?)");
      $this->database->setParameters(array(
        NULL,
        $this->product->getId(),
        $this->product->getMediaId(),
        $this->product->getMediaOrdering()
      ));

      $this->database->run();
    }

    private function insertProductCustomfields() {
      if (!$this->product->getId() || !$this->product->getCustomfields()) {
        return false;
      }

      foreach ($this->product->getCustomfields() as $customfield) {
        $this->database->setQuery("INSERT INTO {$this->tablePrefix}virtuemart_product_customfields VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
        $this->database->setParameters(array(
          NULL,
          $this->product->getId(),
          $customfield->getId(),
          $customfield->getProductCustomValue(),
          $customfield->getPrice(),
          $customfield->getDisabler(),
          $customfield->getOverride(),
          $customfield->getParams(),
          $this->product->getSku(),
          $this->product->getGtin(),
          $this->product->getMpn(),
          $customfield->getPublished(),
          $this->product->getCreatedOn(),
          $this->product->getCreatedBy(),
          $this->product->getModifiedOn(),
          $this->product->getModifiedBy(),
          $this->product->getLockedOn(),
          $this->product->getLockedBy(),
          $this->product->getCustomOrdering()
        ));

        $this->database->run();
      }
    }

    private function insertProductCategory() {
      if (!$this->product->getId()) {
        return false;
      }

      $this->database->setQuery("INSERT INTO {$this->tablePrefix}virtuemart_product_categories VALUES (?,?,?,?)");
      $this->database->setParameters(array(
        NULL,
        $this->product->getId(),
        $this->product->getCategoryId(),
        $this->product->getCatOrdering()
      ));

      $this->database->run();
    }

    public function importCategory($updated = true, $compareField = false) {
      if (!$this->database) {
        return false;
      }
      if ($compareField) {
        $check = $this->checkCategory($compareField);

        if ($check && $updated) {
          $this->updateCategory();
        } else if ($check && !$updated) {
          return false;
        } else {
          $this->insertCategory();
        }
      } else {
        $this->insertCategory();
      }
    }

    private function checkCategory($compareField) {
      $functionName = "get" . ucfirst($compareField);
      $this->database->setQuery("SELECT * FROM {$this->tablePrefix}virtuemart_categories_ru_ru WHERE category_$compareField = ?");
      $this->database->setParameters(array($this->category->$functionName()));

      if ($row = $this->database->run()->fetchObj()->getRow()) {
        $this->category->setId($row->virtuemart_category_id);
        return true;
      } else {
        return false;
      }
    }

    private function updateCategory() {

    }

    private function insertCategory() {
      //main
      $this->insertCategoryMain();

      //meta
      $this->insertCategoryMeta();

      //categoryCategories
      $this->insertCategoryParent();
    }

    private function insertCategoryMain() {
      $this->database->setQuery("INSERT INTO {$this->tablePrefix}virtuemart_categories VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
      $this->database->setParameters(array(
        $this->category->getId(),
        $this->category->getVendorId(),
        $this->category->getTemplate(),
        $this->category->getLayout(),
        $this->category->getProductLayout(),
        $this->category->getProductsPerRow(),
        $this->category->getLimitListStep(),
        $this->category->getLimitListInitial(),
        $this->category->getHits(),
        $this->category->getParams(),
        $this->category->getMetarobot(),
        $this->category->getMetaauthor(),
        $this->category->getOrdering(),
        $this->category->getShared(),
        $this->category->getPublished(),
        $this->category->getCreatedOn(),
        $this->category->getCreatedBy(),
        $this->category->getModifiedOn(),
        $this->category->getModifiedBy(),
        $this->category->getLockedOn(),
        $this->category->getLockedBy()
      ));

      $this->database->run();
      $this->category->setId(intval($this->database->getLastInsertId()));
      ?><pre><?php
      return $this->database->getLastInsertId();
    }

    private function insertCategoryMeta() {
      if (!$this->category->getId()) {
        return false;
      }

      $this->database->setQuery("INSERT INTO {$this->tablePrefix}virtuemart_categories_ru_ru VALUES (?,?,?,?,?,?,?)");
      $this->database->setParameters(array(
        $this->category->getId(),
        $this->category->getName(),
        $this->category->getDescription(),
        $this->category->getMetadesc(),
        $this->category->getMetakey(),
        $this->category->getCustomtitle(),
        $this->category->getSlug()
      ));

      $this->database->run();
    }

    private function insertCategoryParent() {
      if (!$this->category->getId()) {
        return false;
      }

      $this->database->setQuery("INSERT INTO {$this->tablePrefix}virtuemart_category_categories VALUES (?,?,?,?)");
      $this->database->setParameters(array(
        NULL,
        $this->category->getParent(),
        $this->category->getId(),
        $this->category->getParentOrdering()
      ));

      $this->database->run();
    }
  }
?>
