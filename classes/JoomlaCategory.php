<?php
  class JoomlaCategory {
    private $id = NULL;
    private $vendorId = 1;
    private $template = "";
    private $layout = "";
    private $productLayout = "";
    private $productsPerRow = "";
    private $limitListStep = 0;
    private $limitListInitial = 0;
    private $hits = 0;
    private $params = 'show_store_desc=""|showcategory_desc=""|showcategory="0"|categories_per_row="4"|showproducts=""|omitLoaded=""|showsearch=""|productsublayout=""|featured=""|featured_rows=""|omitLoaded_featured=""|discontinued=""|discontinued_rows=""|omitLoaded_discontinued=""|latest=""|latest_rows=""|omitLoaded_latest=""|topten=""|topten_rows=""|omitLoaded_topten=""|recent=""|recent_rows=""|omitLoaded_recent=""|';
    private $metarobot = "";
    private $metaauthor = "";
    private $ordering = 0;
    private $shared = 0;
    private $published = 1;
    private $createdOn;
    private $createdBy = 0;
    private $modifiedOn;
    private $modifiedBy = 0;
    private $lockedOn;
    private $lockedBy = 0;
    private $name = "";
    private $description = "";
    private $metadesc = "";
    private $metakey = "";
    private $customtitle = "";
    private $slug = "";
    private $parent = 0;
    private $parentOrdering = 0;
    private $mediaId = 1;

    public function __construct() {
      $this->createdOn = date("Y-m-d h:i:s");
      $this->modifiedOn = date("Y-m-d h:i:s");
      $this->lockedOn = "1970-01-02";
    }

    public function setId($id) {
      $this->id = $id;
    }

    public function setVendorId($vendorId) {
      $this->vendorId = $vendorId;
    }

    public function setTemplate($template) {
      $this->template = $template;
    }

    public function setLayout($layout) {
      $this->$layout = $layout;
    }

    public function setProductLayout($productLayout) {
      $this->productLayout = $productLayout;
    }

    public function setProductsPerRow($productsPerRow) {
      $this->productsPerRow = $productsPerRow;
    }

    public function setLimitListStep($limitListStep) {
      $this->limitListStep = $limitListStep;
    }

    public function setLimitListInitial($limitListInitial) {
      $this->limitListInitial = $limitListInitial;
    }

    public function setHits($hits) {
      $this->hits = $hits;
    }

    public function setParams($params) {
      $this->params = $params;
    }

    public function setMetarobot($metarobot) {
      $this->metarobot = $metarobot;
    }

    public function setMetaauthor($metaauthor) {
      $this->metaauthor = $metaauthor;
    }

    public function setOrdering($ordering) {
      $this->ordering = $ordering;
    }

    public function setShared($shared) {
      $this->shared = $shared;
    }

    public function setPublished($published) {
      $this->published = $published;
    }

    public function setCreatedOn($createdOn) {
      $this->createdOn = $createdOn;
    }

    public function setCreatedBy($createdBy) {
      $this->createdBy = $createdBy;
    }

    public function setModifiedOn($modifiedOn) {
      $this->modifiedOn = $modifiedOn;
    }

    public function setModifiedBy($modifiedBy) {
      $this->modifiedBy = $modifiedBy;
    }

    public function setLockedOn($lockedOn) {
      $this->lockedOn = $lockedOn;
    }

    public function setLockedBy($lockedBy) {
      $this->lockedBy = $lockedBy;
    }

    public function setName($name) {
      $this->name = $name;
    }

    public function setDescription($description) {
      $this->description = $description;
    }

    public function setMetadesc($metadesc) {
      $this->metadesc = $metadesc;
    }

    public function setMetakey($metakey) {
      $this->metakey = $metakey;
    }

    public function setCustomtitle($customtitle) {
      $this->customtitle = $customtitle;
    }

    public function setSlug($slug) {
      $this->slug = $slug;
    }

    public function setParent($parent) {
      $this->parent = $parent;
    }

    public function setParentOrdering($parentOrdering) {
      $this->parentOrdering = $parentOrdering;
    }

    public function setMediaId($mediaId) {
      $this->mediaId = $mediaId;
    }

    //getters
    public function getId() {
      return $this->id;
    }

    public function getVendorId() {
      return $this->vendorId;
    }

    public function getTemplate() {
      return $this->template;
    }

    public function getLayout() {
      return $this->layout;
    }

    public function getProductLayout() {
      return $this->productLayout;
    }

    public function getProductsPerRow() {
      return $this->productsPerRow;
    }

    public function getLimitListStep() {
      return $this->limitListStep;
    }

    public function getLimitListInitial() {
      return $this->limitListInitial;
    }

    public function getHits() {
      return $this->hits;
    }

    public function getParams() {
      return $this->params;
    }

    public function getMetarobot() {
      return $this->metarobot;
    }

    public function getMetaauthor() {
      return $this->metaauthor;
    }

    public function getOrdering() {
      return $this->ordering;
    }

    public function getShared() {
      return $this->shared;
    }

    public function getPublished() {
      return $this->published;
    }

    public function getCreatedOn() {
      return $this->createdOn;
    }

    public function getCreatedBy() {
      return $this->createdBy;
    }

    public function getModifiedOn() {
      return $this->modifiedOn;
    }

    public function getModifiedBy() {
      return $this->modifiedBy;
    }

    public function getLockedOn() {
      return $this->lockedOn;
    }

    public function getLockedBy() {
      return $this->lockedBy;
    }

    public function getName() {
      return $this->name;
    }

    public function getDescription() {
      return $this->description;
    }

    public function getMetadesc() {
      return $this->metadesc;
    }

    public function getMetakey() {
      return $this->metakey;
    }

    public function getCustomtitle() {
      return $this->customtitle;
    }

    public function getSlug() {
      return $this->slug;
    }

    public function getParent() {
      return $this->parent;
    }

    public function getParentOrdering() {
      return $this->parentOrdering;
    }

    public function getMediaId() {
      return $this->mediaId;
    }
  }
?>
