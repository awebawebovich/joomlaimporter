<?php
  class Database {
    private $lastRow;
    private $lastInsertId;
    private $query = false;
    private $queryHandle = false;
    private $parameters = array();
    private $connect;
    private $debug;

    public function __construct($host, $dbname, $user, $password, $charset, $debug = false) {
      $this->debug = $debug;
      try {
        $dns = "mysql:host=$host;dbname=$dbname;charset=$charset";

        $this->connect = new PDO($dns, $user, $password);

        if (!$this->connect) {
          throw new PDOException("Couldn`t connect to database");
        }
      } catch (PDOException $e) {
        if ($this->debug) {
          print $e->getMessage() . "<br>";
        }

        $this->connect = false;
      }
    }

    public function getConnect() {
      return $this->connect;
    }

    public function getRow() {
      return $this->nextRow;
    }

    public function fetchObj() {
      if ($this->queryHandle && is_object($this->queryHandle)) {
        try {
          $this->nextRow = $this->queryHandle->fetch(PDO::FETCH_OBJ);
        } catch (PDOException $e) {
          if ($this->debug) {
            print $e->getMessage() . "<br>";
          }
          $this->nextRow = false;
        }
      } else {
        $this->nextRow = false;
      }

      return $this;
    }

    public function fetchAssoc() {
      if ($this->queryHandle && is_object($this->queryHandle)) {
        try {
          $this->nextRow = $this->queryHandle->fetch(PDO::FETCH_ASSOC);
        } catch (PDOException $e) {
          if ($this->debug) {
            print $e->getMessage() . "<br>";
          }
          $this->nextRow = false;
        }
      } else {
        $this->nextRow = false;
      }

      return $this;
    }

    public function getLastInsertId() {
      if ($this->connect && is_object($this->connect)) {
        try {
          return $this->connect->lastInsertId();
        } catch (PDOException $e) {
          if ($this->debug) {
            print $e->getMessage() . "<br>";
          }
          return false;
        }
      } else {
        return false;
      }
    }

    public function setQuery($query) {
      $this->query = $query;

      return $this;
    }

    public function setParameters($parameters) {
      if (is_array($parameters)) {
        $this->parameters = $parameters;
      }

      return $this;
    }

    public function run() {
      if (empty($this->query) || !$this->connect) {
        return false;
      }

      try {
        $this->queryHandle = $this->connect->prepare($this->query);
        $this->queryHandle->execute($this->parameters);

        if (!$this->queryHandle) {
          throw new PDOException("Couldn`t run query: {$this->query}");
        }
      } catch (PDOException $e) {
        if ($this->debug) {
          print $e->getMessage() . "<br>";
        }
        $this->queryHandle = false;
      }

      return $this;
    }
  }
?>
