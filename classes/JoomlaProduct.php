<?php
  class JoomlaProduct {
    private $id = NULL;
    private $vendorId = 1;
    private $parent = 0;
    private $sku = "";
    private $gtin = "";
    private $mpn = "";
    private $weight = NULL;
    private $weightUom = "KG";
    private $length = NULL;
    private $width = NULL;
    private $height = NULL;
    private $lwhUom = "M";
    private $url = NULL;
    private $inStock = 0;
    private $ordered = 0;
    private $stockhandle = 0;
    private $stockNotification = 0;
    private $availableData;
    private $avaibility = "";
    private $special = 0;
    private $discontinued = 0;
    private $sales = 0;
    private $unit = "KG";
    private $packing = NULL;
    private $params = 'min_order_level=""|max_order_level=""|step_order_level=""|product_box=""|';
    private $hits = NULL;
    private $intnotes = "";
    private $metarobot = "";
    private $metaauthor = "";
    private $layout = "";
    private $published = 1;
    private $pordering = 0;
    private $createdOn;
    private $createdBy = 0;
    private $modifiedOn;
    private $modifiedBy = 0;
    private $lockedOn;
    private $lockedBy = 0;
    private $sDesc = "";
    private $desc = "";
    private $name = "";
    private $metadesc = "";
    private $metakey = "";
    private $customtitle = "";
    private $slug = "";
    private $categoryId = 0;
    private $catOrdering = 0;
    private $customfields = NULL;
    private $manufacturerId = 0;
    private $mediaId = 0;
    private $mediaOrdering = 0;
    private $shoppergroupId = 0;
    private $price = 0;
    private $override = 0;
    private $overridePrice = 0;
    private $taxId = 0;
    private $discountId = 0;
    private $currency = 131;
    private $pricePublishUp;
    private $pricePublishDown;
    private $priceQuantityStart = 0;
    private $priceQuantityEnd = 0;
    private $priceCreatedOn;
    private $priceCreatedBy = 0;
    private $priceModifiedOn;
    private $priceModifiedBy = 0;
    private $priceLockedOn;
    private $priceLockedBy = 0;
    private $customOrdering = 0;

    public function __construct() {
      $this->createdOn = date("Y-m-d h:i:s");
      $this->modifiedOn = date("Y-m-d h:i:s");
      $this->lockedOn = strtotime("0000-00-00 00:00:00");
      $this->pricePublishUp = strtotime("0000-00-00 00:00:00");
      $this->pricePublishDown = strtotime("0000-00-00 00:00:00");
      $this->priceCreatedOn = date("Y-m-d h:i:s");
      $this->priceModifiedOn = date("Y-m-d h:i:s");
      $this->priceLockedOn = strtotime("0000-00-00 00:00:00");
      $this->availableData = date("Y-m-d h:i:s");
    }

    public function setId($id) {
      $this->id = $id;
    }

    public function setVendorId($vendorId) {
      $this->vendorId = $vendorId;
    }

    public function setParent($parent) {
      $this->parent = $parent;
    }

    public function setSku($sku) {
      $this->sku = $sku;
    }

    public function setGtin($gtin) {
      $this->gtin = $gtin;
    }

    public function setMpn($mpn) {
      $this->mpn = $mpn;
    }

    public function setWeight($weight) {
      $this->weight = $weight;
    }

    public function setWeightUom($weightUom) {
      $this->weightUom = $weightUom;
    }

    public function setLength($length) {
      $this->length = $length;
    }

    public function setWidth($width) {
      $this->width = $width;
    }

    public function setHeight($height) {
      $this->height = $height;
    }

    public function setlwhUom($lwhUom) {
      $this->lwhUom = $lwhUom;
    }

    public function setUrl($url) {
      $this->url = $url;
    }

    public function setInStock($inStock) {
      $this->inStock = $inStock;
    }

    public function setOrdered($ordered) {
      $this->ordered = $ordered;
    }

    public function setStockhandle($stockhandle) {
      $this->stockhandle = $stockhandle;
    }

    public function setStockNotification($stockNotification) {
      $this->stockNotification = $stockNotification;
    }

    public function setAvailableData($availableData) {
      $this->availableData = $availableData;
    }

    public function setAvaibility($avaibility) {
      $this->avaibility = $avaibility;
    }

    public function setSpecial($special) {
      $this->special = $special;
    }

    public function setDiscontinued($discontinued) {
      $this->discontinued = $discontinued;
    }

    public function setSales($sales) {
      $this->sales = $sales;
    }

    public function setUnit($unit) {
      $this->unit = $unit;
    }

    public function setPacking($packing) {
      $this->packing = $packing;
    }

    public function setParams($params) {
      $this->params = $params;
    }

    public function setHits($hits) {
      $this->hits = $hits;
    }

    public function setIntnotes($intnotes) {
      $this->intnotes = $intnotes;
    }

    public function setMetarobot($metarobot) {
      $this->metarobot = $metarobot;
    }

    public function setMetaauthor($metaauthor) {
      $this->metaauthor = $metaauthor;
    }

    public function setLayout($layout) {
      $this->layout = $layout;
    }

    public function setPublished($published) {
      $this->published = $published;
    }

    public function setPordering($pordering) {
      $this->pordering = $pordering;
    }

    public function setCreadedOn($createdOn) {
      $this->createdOn = $createdOn;
    }

    public function setCreatedBy($createdBy) {
      $this->createdBy = $createdBy;
    }

    public function setModifiedOn($modifiedOn) {
      $this->modifiedOn = $modifiedOn;
    }

    public function setModifiedBy($modifiedBy) {
      $this->modifiedBy = $modifiedBy;
    }

    public function setLockedOn($lockedOn) {
      $this->lockedOn = $lockedOn;
    }

    public function setLockedBy($lockedBy) {
      $this->lockedBy = $lockedBy;
    }

    public function setSDesc($sDesc) {
      $this->sDesc = $sDesc;
    }

    public function setDesc($desc) {
      $this->desc = $desc;
    }

    public function setName($name) {
      $this->name = $name;
    }

    public function setMetadesc($metadesc) {
      $this->metadesc = $metadesc;
    }

    public function setMetakey($metakey) {
      $this->metakey = $metakey;
    }

    public function setCustomtitle($customtitle) {
      $this->customtitle = $customtitle;
    }

    public function setSlug($slug) {
      $this->slug = $slug;
    }

    public function setCategoryId($categoryId) {
      $this->categoryId = $categoryId;
    }

    public function setCatOrdering($catOrdering) {
      $this->catOrdering = $catOrdering;
    }

    public function setCustomfields($customfields) {
      $this->customfields = $customfields;
    }

    public function setManufacturerId($manufacturerId) {
      $this->manufacturerId = $manufacturerId;
    }

    public function setMediaId($mediaId) {
      $this->mediaId = $mediaId;
    }

    public function setMediaOrdering($mediaOrdering) {
      $this->mediaOrdering = $mediaOrdering;
    }

    public function setShoppergroupId($shoppergroupId) {
      $this->shoppergroupId = $shoppergroupId;
    }

    public function setPrice($price) {
      $this->price = $price;
    }

    public function setOverride($override) {
      $this->override = $override;
    }

    public function setOverridePrice($overridePrice) {
      $this->overridePrice = $overridePrice;
    }

    public function setTaxId($taxId) {
      $this->taxId = $taxId;
    }

    public function setDiscountId($discountId) {
      $this->discountId = $discountId;
    }

    public function setCurrency($currency) {
      $this->currency = $currency;
    }

    public function setPricePublishUp($pricePublishUp) {
      $this->pricePublishUp = $pricePublishUp;
    }

    public function setPricePublishDown($pricePublishDown) {
      $this->pricePublishDown = $pricePublishDown;
    }

    public function setPriceQuantityStart($priceQuantityStart) {
      $this->priceQuantityStart = $priceQuantityStart;
    }

    public function setPriceQuantityEnd($priceQuantityEnd) {
      $this->priceQuantityEnd = $priceQuantityEnd;
    }

    public function setPriceCreatedOn($priceCreatedOn) {
      $this->priceCreatedOn = $priceCreatedOn;
    }

    public function setPriceCreatedBy($priceCreatedBy) {
      $this->priceCreatedBy = $priceCreatedBy;
    }

    public function setPriceModifiedOn($priceModifiedOn) {
      $this->priceModifiedOn = $priceModifiedOn;
    }

    public function setPriceModifiedBy($priceModifiedBy) {
      $this->priceModifiedBy = $priceModifiedBy;
    }

    public function setPriceLockedOn($priceLockedOn) {
      $this->priceLockedOn = $priceLockedOn;
    }

    public function setPriceLockedBy($priceLockedBy) {
      $this->priceLockedBy = $priceLockedBy;
    }

    public function setCustomOrdering($customOrdering) {
      $this->customOrdering = $customOrdering;
    }

    //getters
    public function getId() {
      return $this->id;
    }

    public function getVendorId() {
      return $this->vendorId;
    }

    public function getParent() {
      return $this->parent;
    }

    public function getSku() {
      return $this->sku;
    }

    public function getGtin() {
      return $this->gtin;
    }

    public function getMpn() {
      return $this->mpn;
    }

    public function getWeight() {
      return $this->weight;
    }

    public function getWeightUom() {
      return $this->weightUom;
    }

    public function getLength() {
      return $this->length;
    }

    public function getWidth() {
      return $this->width;
    }

    public function getHeight() {
      return $this->height;
    }

    public function getlwhUom() {
      return $this->lwhUom;
    }

    public function getUrl() {
      return $this->url;
    }

    public function getInStock() {
      return $this->inStock;
    }

    public function getOrdered() {
      return $this->ordered;
    }

    public function getStockhandle() {
      return $this->stockhandle;
    }

    public function getStockNotification() {
      return $this->stockNotification;
    }

    public function getAvailableData() {
      return $this->availableData;
    }

    public function getAvaibility() {
      return $this->avaibility;
    }

    public function getSpecial() {
      return $this->special;
    }

    public function getDiscontinued() {
      return $this->discontinued;
    }

    public function getSales() {
      return $this->sales;
    }

    public function getUnit() {
      return $this->unit;
    }

    public function getPacking() {
      return $this->packing;
    }

    public function getParams() {
      return $this->params;
    }

    public function getHits() {
      return $this->hits;
    }

    public function getIntnotes() {
      return $this->intnotes;
    }

    public function getMetarobot() {
      return $this->metarobot;
    }

    public function getMetaauthor() {
      return $this->metaauthor;
    }

    public function getLayout() {
      return $this->layout;
    }

    public function getPublished() {
      return $this->published;
    }

    public function getPordering() {
      return $this->pordering;
    }

    public function getCreatedOn() {
      return $this->createdOn;
    }

    public function getCreatedBy() {
      return $this->createdBy;
    }

    public function getModifiedOn() {
      return $this->modifiedOn;
    }

    public function getModifiedBy() {
      return $this->modifiedBy;
    }

    public function getLockedOn() {
      return $this->lockedOn;
    }

    public function getLockedBy() {
      return $this->lockedBy;
    }

    public function getSDesc() {
      return $this->sDesc;
    }

    public function getDesc() {
      return $this->desc;
    }

    public function getName() {
      return $this->name;
    }

    public function getMetadesc() {
      return $this->metadesc;
    }

    public function getMetakey() {
      return $this->metakey;
    }

    public function getCustomtitle() {
      return $this->customtitle;
    }

    public function getSlug() {
      return $this->slug;
    }

    public function getCategoryId() {
      return $this->categoryId;
    }

    public function getCatOrdering() {
      return $this->catOrdering;
    }

    public function getCustomfields() {
      return $this->customfields;
    }

    public function getManufacturerId() {
      return $this->manufacturerId;
    }

    public function getMediaId() {
      return $this->mediaId;
    }

    public function getMediaOrdering() {
      return $this->mediaOrdering;
    }

    public function getShoppergroupId() {
      return $this->shoppergroupId;
    }

    public function getPrice() {
      return $this->price;
    }

    public function getOverride() {
      return $this->override;
    }

    public function getOverridePrice() {
      return $this->overridePrice;
    }

    public function getTaxId() {
      return $this->taxId;
    }

    public function getDiscountId() {
      return $this->discountId;
    }

    public function getCurrency() {
      return $this->currency;
    }

    public function getPricePublishUp() {
      return $this->pricePublishUp;
    }

    public function getPricePublishDown() {
      return $this->pricePublishDown;
    }

    public function getPriceQuantityStart() {
      return $this->priceQuantityStart;
    }

    public function getPriceQuantityEnd() {
      return $this->priceQuantityEnd;
    }

    public function getPriceCreatedOn() {
      return $this->priceCreatedOn;
    }

    public function getPriceCreatedBy() {
      return $this->priceCreatedBy;
    }

    public function getPriceModifiedOn() {
      return $this->priceModifiedOn;
    }

    public function getPriceModifiedBy() {
      return $this->priceModifiedBy;
    }

    public function getPriceLockedOn() {
      return $this->priceLockedOn;
    }

    public function getPriceLockedBy() {
      return $this->priceLockedBy;
    }

    public function getCustomOrdering() {
      return $this->customOrdering;
    }
  }
?>
