<?php
  class JoomlaCustomfield {
    private $id = NULL;
    private $parent = 0;
    private $vendorId = 0;
    private $jpluginId = 0;
    private $element = 0;
    private $adminOnly = 0;
    private $title = "";
    private $showTitle = 1;
    private $tip = "";
    private $value = NULL;
    private $desc = "";
    private $fieldType = "S";
    private $isList = 0;
    private $isHidden = 0;
    private $isCartAttribute = 0;
    private $isInput = 0;
    private $searchable = 0;
    private $layoutPos = 0;
    private $params = 'wPrice="1"|wImage="1"|wDescr="1"|';
    private $shared = 0;
    private $published = 1;
    private $createdOn;
    private $createdBy = 0;
    private $ordering = 0;
    private $modifiedOn;
    private $modifiedBy = 0;
    private $lockedOn;
    private $lockedBy = 0;
    private $productCustomValue = NULL;
    private $disabler = 0;
    private $override = 0;
    private $price = 0;

    public function __construct() {
      $this->createdOn = date("Y-m-d h:i:s");
      $this->modifiedOn = date("Y-m-d h:i:s");
      $this->lockedOn = strtotime("0000-00-00 00:00:00");
    }

    public function setProductCustomValue($productCustomValue) {
      $this->productCustomValue = $productCustomValue;
    }

    public function setId($id) {
      $this->id = $id;
    }

    public function setParent($parent) {
      $this->parent = $parent;
    }

    public function setVendorId($vendorId) {
      $this->vendorId = $vendorId;
    }

    public function setJpluginId($jpluginId) {
      $this->jpluginId = $jpluginId;
    }

    public function setElement($element) {
      $this->element = $element;
    }

    public function setAdminOnly($adminOnly) {
      $this->adminOnly = $adminOnly;
    }

    public function setTitle($title) {
      $this->title = $title;
    }

    public function setShowTitle($showTitle) {
      $this->showTitle = $showTitle;
    }

    public function setTip($tip) {
      $this->tip = $tip;
    }

    public function setValue($value) {
      $this->value = $value;
    }

    public function setDesc($desc) {
      $this->desc = $desc;
    }

    public function setFieldType($fieldType) {
      $this->fieldType = $fieldType;
    }

    public function setIsList($isList) {
      $this->isList = $isList;
    }

    public function setIsHidden($isHidden) {
      $this->isHidden = $isHidden;
    }

    public function setIsCartAttribute($isCartAttribute) {
      $this->isCartAttribute = $isCartAttribute;
    }

    public function setIsInput($isInput) {
      $this->isInput = $isInput;
    }

    public function setSearchable($searchable) {
      $this->searchable = $searchable;
    }

    public function setLayoutPos($layoutPos) {
      $this->layoutPos = $layoutPos;
    }

    public function setParams($params) {
      $this->params = $params;
    }

    public function setShared($shared) {
      $this->shared = $shared;
    }

    public function setPublished($published) {
      $this->published = $published;
    }

    public function setCreatedOn($createdOn) {
      $this->createdOn = $createdOn;
    }

    public function setCreatedBy($createdBy) {
      $this->createdBy = $createdBy;
    }

    public function setOrdering($ordering) {
      $this->ordering = $ordering;
    }

    public function setModifiedOn($modifiedOn) {
      $this->modifiedOn = $modifiedOn;
    }

    public function setModifiedBy($modifiedBy)  {
      $this->modifiedBy = $modifiedBy;
    }

    public function setLockedOn($lockedOn)  {
      $this->lockedOn = $lockedOn;
    }

    public function setLockedBy($lockedBy)  {
      $this->lockedBy = $lockedBy;
    }

    public function setDisabler($disabler)  {
      $this->disabler = $disabler;
    }

    public function setOverride($override)  {
      $this->override = $override;
    }

    public function setPrice($price)  {
      $this->price = $price;
    }

    //getters
    public function getId() {
      return $this->id;
    }

    public function getParent() {
      return $this->parent;
    }

    public function getVendorId() {
      return $this->vendorId;
    }

    public function getJpluginId() {
      return $this->jpluginId;
    }

    public function getElement() {
      return $this->element;
    }

    public function getAdminOnly() {
      return $this->adminOnly;
    }

    public function getTitle() {
      return $this->title;
    }

    public function getShowTitle() {
      return $this->showTitle;
    }

    public function getTip() {
      return $this->tip;
    }

    public function getValue() {
      return $this->value;
    }

    public function getDesc() {
      return $this->desc;
    }

    public function getFieldType() {
      return $this->fieldType;
    }

    public function getIsList() {
      return $this->isList;
    }

    public function getIsHidden() {
      return $this->isHidden;
    }

    public function getIsCartAttribute() {
      return $this->isCartAttribute;
    }

    public function getIsInput() {
      return $this->isInput;
    }

    public function getSearchable() {
      return $this->searchable;
    }

    public function getLayoutPos() {
      return $this->layoutPos;
    }

    public function getParams() {
      return $this->params;
    }

    public function getShared() {
      return $this->shared;
    }

    public function getPublished() {
      return $this->published;
    }

    public function getCreatedOn() {
      return $this->createdOn;
    }

    public function getCreatedBy() {
      return $this->createdBy;
    }

    public function getOrdering() {
      return $this->ordering;
    }

    public function getModifiedOn() {
      return $this->modifiedOn;
    }

    public function getModifiedBy()  {
      return $this->modifiedBy;
    }

    public function getLockedOn()  {
      return $this->lockedOn;
    }

    public function getLockedBy()  {
      return $this->lockedBy;
    }

    public function getDisabler()  {
      return $this->disabler;
    }

    public function getOverride()  {
      return $this->override;
    }

    public function getPrice()  {
      return $this->price;
    }

    public function getProductCustomValue() {
      return $this->productCustomValue;
    }
  }
?>
