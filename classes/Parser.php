<?php
  class Parser {
    private $workingPage;
    private $products;
    private $productHandler;
    private $nameHandler;
    private $descriptionHandler;
    private $skuHandler;
    private $priceHandler;
    private $imageHandler;
    private $attributesHandler;
    private $productBlock = NULL;
    private $productLink = NULL;
    private $linkHandler = NULL;
    private $paginationHandler = NULL;
    private $nextPaginationLink = NULL;
    private $paginationCurrentHandler = NULL;
    private $siteUrl = NULL;

    public function __construct($workingPage = "") {
      if ($workingPage) {
        $this->workingPage = $workingPage;

        $this->nameHandler = array();
        $this->descriptionHandler = array();
        $this->skuHandler = array();
        $this->priceHandler = array();
        $this->imageHandler = array();
        $this->attributesHandler = array();

        $url = explode("/", $this->workingPage);
        if (strrpos($this->workingPage, "https://") !== false || strrpos($this->workingPage, "http://") !== false) {
          $this->siteUrl = $url[2];
        } else {
          $this->siteUrl = $url[0];
        }
      }
    }

    public function setSiteUrl($siteUrl) {
      $this->siteUrl = $siteUrl;
    }

    public function setNameHandler($handler, $needOpenCard = false, $isHtml = false) {
      $this->nameHandler["selector"] = $handler;
      $this->nameHandler["needOpenCard"] = $needOpenCard;
      $this->nameHandler["isHtml"] = $isHtml;
    }

    public function setDescriptionHandler($handler, $needOpenCard = false, $isHtml = false) {
      $this->descriptionHandler["selector"] = $handler;
      $this->descriptionHandler["needOpenCard"] = $needOpenCard;
      $this->descriptionHandler["isHtml"] = $isHtml;
    }

    public function setSkuHandler($handler, $needOpenCard = false, $isHtml = false) {
      $this->skuHandler["selector"] = $handler;
      $this->skuHandler["needOpenCard"] = $needOpenCard;
      $this->skuHandler["isHtml"] = $isHtml;
    }

    public function setPriceHandler($handler, $needOpenCard = false, $isHtml = false) {
      $this->priceHandler["selector"] = $handler;
      $this->priceHandler["needOpenCard"] = $needOpenCard;
      $this->priceHandler["isHtml"] = $isHtml;
    }

    public function setImageHandler($handler, $needOpenCard = false, $uploadFolder = false) {
      $this->imageHandler["selector"] = $handler;
      $this->imageHandler["needOpenCard"] = $needOpenCard;
      $this->imageHandler["uploadFolder"] = $uploadFolder;
    }

    public function setAttributesHandler($handler, $needOpenCard = false, $isHtml = false, $partPageLink = false) {
      $this->attributesHandler["selector"] = $handler;
      $this->attributesHandler["needOpenCard"] = $needOpenCard;
      $this->attributesHandler["isHtml"] = $isHtml;
      $this->attributesHandler["partPageLink"] = $partPageLink;
    }

    public function setLinkHandler($linkHandler) {
      $this->linkHandler = $linkHandler;
    }

    public function setPaginationHandler($paginationHandler) {
      $this->paginationHandler = $paginationHandler;
    }

    public function setPaginationCurrentHandler($paginationCurrentHandler) {
      $this->paginationCurrentHandler = $paginationCurrentHandler;
    }

    public function setProductHandler($productHandler) {
      $this->productHandler = $productHandler;
    }

    private function setProductBlock($productBlock) {
      $this->productBlock = $productBlock;
    }

    public function setWorkingPage($workingPage) {
      $this->workingPage = $workingPage;
    }

    public function getProducts() {
      return $this->products;
    }

    private function loadPage($link) {
      $curl = curl_init();
      curl_setopt($curl, CURLOPT_URL, $link);
      curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 10);
      $html = curl_exec($curl);
      curl_close($curl);

      return str_get_html($html);
    }

    public function parse() {
      $data = array();
      $i = 0;

      do {
        $page = $this->loadPage($this->workingPage);

        if (!$page) {
          break;
        }

        if (!$products = $page->find($this->productHandler)) {
          break;
        }

        foreach ($products as $product) {
          $this->productBlock = $product;

          if ($this->linkHandler) {
             $tempLink = $this->productBlock->find($this->linkHandler);

             if ($tempLink) {
              $data[$i]['link'] = $tempLink[0]->href;
              $this->productLink = $tempLink[0]->href;
             } else {
              $data[$i]['link'] = false;
              $this->productLink = false;
             }
          }

          $data[$i]['name'] = $this->parseInfo($this->nameHandler);
          $data[$i]['description'] = $this->parseInfo($this->descriptionHandler);
          $data[$i]['sku'] = $this->parseInfo($this->skuHandler);
          $data[$i]['image'] = $this->parseImage();
          $data[$i]['price'] = $this->parseInfo($this->priceHandler);
          $data[$i]['attributes'] = $this->parseInfo($this->attributesHandler);

          ?><pre><?php
          print "Номер: $i<br>";
          var_dump($data[$i]);
          ob_flush();
          flush();

          $i++;
        }
      } while($this->workingPage = $this->checkPagination($page));

      $this->products = $data;
    }

    private function parseInfo($handler) {
      if (!$this->productBlock || !$handler["selector"]) {
        return false;
      }

      if ($handler["needOpenCard"]) {
        if (!$this->productLink) {
          return false;
        } else {
          if (strrpos($this->productLink, $this->siteUrl) === false) {
            $this->productLink = $this->siteUrl . $this->productLink;
          }

          if (isset($handler['partPageLink'])) {
            $link = $this->productLink . $handler['partPageLink'];
          } else {
            $link = $this->productLink;
          }

          $productPage = $this->loadPage($link);

          $tempBlock = $productPage->find($handler["selector"]);

          if (!$tempBlock) {
            return false;
          } else {
            if ($handler["isHtml"]) {
              return $tempBlock[0]->innertext;
            } else {
              return $tempBlock[0]->plaintext;
            }
          }
        }
      } else {
        $tempBlock = $this->productBlock->find($handler["selector"]);

        if (!$tempBlock) {
          return false;
        } else {
          if ($handler["isHtml"]) {
            return $tempBlock[0]->innertext;
          } else {
            return $tempBlock[0]->plaintext;
          }
        }
      }
    }

    private function parseImage() {
      if (!$this->imageHandler['selector']) {
        return false;
      }

      if ($this->imageHandler["needOpenCard"]) {
        if (!$this->productLink) {
          return false;
        } else {
          if (strrpos($this->productLink, $this->siteUrl) === false) {
            $this->productLink = $this->siteUrl . $this->productLink;
          }

          $productPage = $this->loadPage($this->productLink);

          if (!$productPage) {
            return false;
          }

          $tempBlock = $productPage->find($this->imageHandler["selector"]);

          if (!$tempBlock) {
            return false;
          }
        }
      } else {
        if (!$this->productBlock) {
          return false;
        } else {
          $tempBlock = $this->productBlock->find($this->imageHandler["selector"]);

          if (!$tempBlock) {
            return false;
          }
        }
      }

      $imageLink = $tempBlock[0]->src;

      if (!$imageLink) {
        return false;
      } else {
        return $this->loadImage($imageLink);
      }
    }

    private function parseAttributes() {
      if ($this->attributesHandler["partPageLink"]) {
        if (!$this->productLink) {
          return false;
        } else {
          $link = $this->productLink . $this->attributesHandler["partPageLink"];
        }
      }

      if (!$this->productBlock || !$this->attributesHandler["selector"]) {
        return false;
      }

      if ($this->attributesHandler["needOpenCard"]) {
        if (strrpos($link, $this->siteUrl) === false) {
          $link = $this->siteUrl . $link;
        }

        $productPage = $this->loadPage($link);

        if (!$productPage) {
          return false;
        }

        $tempBlock = $productPage->find($this->attributesHandler["selector"]);

        if (!$tempBlock) {
          return false;
        } else {
          if (!$this->attributesHandler["attributeSelector"]) {
            if ($this->attributesHandler["isHtml"]) {
              return $tempBlock[0]->innertext;
            } else {
              return $tempBlock[0]->plaintext;
            }
          } else {
            $attributeBlocks = $tempBlock[0]->find($this->attributesHandler["attributeSelector"]);

            if (!$attributeBlocks) {
              return false;
            } else {
              $attributes = array();
              foreach ($attributeBlocks as $attribute) {
                if ($this->attributesHandler['isHtml']) {
                  $attributes[] = $attribute->innertext;
                } else {
                  $attributes[] = $attribute->palintext;
                }
              }

              return $attributes;
            }
          }
        }
      } else {
        $tempBlock = $this->productBlock->find($this->attributesHandler["selector"]);

        if (!$tempBlock) {
          return false;
        } else {
          if ($this->attributesHandler["isHtml"]) {
            return $tempBlock[0]->innertext;
          } else {
            return $tempBlock[0]->plaintext;
          }
        }
      }
    }

    private function checkPagination($page) {
      if (!$page) {
        return false;
      }

      if ($pagination = $page->find($this->paginationHandler . " " . $this->paginationCurrentHandler)) {
        if ($pagination[0]->next_sibling()) {

          $nextPage = $pagination[0]->next_sibling()->find("a")[0]->href;
          if (!$nextPage) {
            return false;
          }

          if (strrpos($nextPage, $this->siteUrl) === false) {
            return $this->siteUrl . $nextPage;
          } else {
            return $nextPage;
          }
        } else {
          return false;
        }
      } else {
        return false;
      }
    }

    private function loadImage($imageLink) {
      if (strrpos($imageLink, $this->siteUrl) === false) {
        $imageLink = $this->siteUrl . $imageLink;
      }

      $image = file_get_contents($imageLink);

      if (!$image) {
        return false;
      } else {
        $result = file_put_contents($this->imageHandler["uploadFolder"] . basename($imageLink), $image);

        if ($result) {
          return basename($imageLink);
        } else {
          return false;
        }
      }
    }
  }
?>
