<?php
  return array(
    "Воздуховоды" => "https://www.rusklimat.ru/ventilyatsiya/vozdukhovody/",
    "Вентиляционные решетки и диффузоры" => "https://www.rusklimat.ru/ventilyatsiya/ventilyatsionnye_reshetki_i_diffuzory/",
    "Канальные нагреватели и охладители" => "https://www.rusklimat.ru/ventilyatsiya/kanalnye_nagrevateli_i_okhladiteli/",
    "Вентиляторы" => "https://www.rusklimat.ru/ventilyatsiya/ventilyatoryi/",
    "Автоматика для вентиляции" => "https://www.rusklimat.ru/ventilyatsiya/avtomatika_dlya_ventilyatsii/",
    "Сетевое оборудование" => "https://www.rusklimat.ru/ventilyatsiya/setevoe-oborudovanie/",
    "Кондиционеры" => "https://www.rusklimat.ru/konditsionery/split-sistemy/",
    "Мульти-сплит системы" => "https://www.rusklimat.ru/konditsionery/multi-split-sistemy-s-odnim-vneshnim-blokom/"
  );
?>
